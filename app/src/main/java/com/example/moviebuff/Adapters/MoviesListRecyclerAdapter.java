package com.example.moviebuff.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviebuff.MainActivity;
import com.example.moviebuff.ModelClass.MovieDetails;
import com.example.moviebuff.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MoviesListRecyclerAdapter extends RecyclerView.Adapter<MoviesListRecyclerAdapter.ViewHolder> {
    ArrayList<MovieDetails> MOVIES_DETAILS;

    MainActivity mainActivity;
    ApplicationsAdapterclick adapterclick;


    public MoviesListRecyclerAdapter(MainActivity mainActivity,ArrayList<MovieDetails> MOVIES_DETAILS) {

        this.mainActivity= mainActivity;
        this.MOVIES_DETAILS=MOVIES_DETAILS;

        try
        {
            this.adapterclick=((ApplicationsAdapterclick) mainActivity);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.movielists, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Picasso.with(mainActivity).load("https://image.tmdb.org/t/p/" +
                "w500" +MOVIES_DETAILS.get(position).getPoster_path()).placeholder(R.drawable.placeholder).fit().centerCrop().into(holder.movies_poster_imageview);
        holder.movie_name_tv.setText(MOVIES_DETAILS.get(position).getOriginal_title());
        holder.movie_language_tv.setText(MOVIES_DETAILS.get(position).getOriginal_language());
        holder.movie_release_date_tv.setText(MOVIES_DETAILS.get(position).getRelease_date());
    }

    @Override
    public int getItemCount() {
        return MOVIES_DETAILS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView movies_poster_imageview;
        public TextView movie_name_tv;
        public TextView movie_language_tv;
        public TextView movie_release_date_tv;

        public ViewHolder(View list) {
            super(list);

            this.movies_poster_imageview=list.findViewById(R.id.Movie_poster);
            this.movie_name_tv=list.findViewById(R.id.Movie_name);
            this.movie_language_tv=list.findViewById(R.id.Movie_Language);
            this.movie_release_date_tv=list.findViewById(R.id.Movie_release_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface ApplicationsAdapterclick
    {
        public void recyclerviewclick(int position);
    }
}

