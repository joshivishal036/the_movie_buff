package com.example.moviebuff.APIs;



import com.example.moviebuff.ModelClass.Movies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @SuppressWarnings("ALL")
    @GET("movie/popular")
    Call<Movies> getPopularMovies(@Query("page") int page,@Query("api_key") String userkey);

}
