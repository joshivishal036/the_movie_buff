package com.example.moviebuff;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MovieDetailsActivity extends AppCompatActivity {

    TextView ratings_textview, about_textview;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageView=findViewById(R.id.back_poster);
        ratings_textview=findViewById(R.id.ratings_tv);
        about_textview=findViewById(R.id.about_movie_tv);

        Picasso.with(MovieDetailsActivity.this).load("https://image.tmdb.org/t/p/" +
                "w500" +getIntent().getStringExtra("back_poster")).placeholder(R.drawable.placeholde2).fit().centerCrop().into(imageView);
        ratings_textview.setText(getIntent().getStringExtra("ratings")+"/10");
        about_textview.setText(getIntent().getStringExtra("about"));
    }

}
