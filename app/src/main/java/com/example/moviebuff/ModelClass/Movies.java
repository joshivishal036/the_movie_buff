package com.example.moviebuff.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Movies {

    @SerializedName("success")
    private boolean success;

    @SerializedName("status_message")
    private String staus_message;

    @SerializedName("results")
    private ArrayList<MovieDetails> results;

    public Movies(boolean success, String staus_message, ArrayList<MovieDetails> results) {
        this.success = success;
        this.staus_message = staus_message;
        this.results = results;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getStaus_message() {
        return staus_message;
    }

    public ArrayList<MovieDetails> getResults() {
        return results;
    }
}
