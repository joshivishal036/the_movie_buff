package com.example.moviebuff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.example.moviebuff.APIs.Api;
import com.example.moviebuff.APIs.ApiService;
import com.example.moviebuff.Adapters.MoviesListRecyclerAdapter;
import com.example.moviebuff.ModelClass.MovieDetails;
import com.example.moviebuff.ModelClass.Movies;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements MoviesListRecyclerAdapter.ApplicationsAdapterclick {

    RecyclerView recyclerView;
    Button button;
    TextView textView;

    ArrayList<MovieDetails> MOVIES_DETAILS=new ArrayList<>();
//    ArrayList<String> MOVIES_LANGUAGE=new ArrayList<>();

    MoviesListRecyclerAdapter moviesListRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView=findViewById(R.id.Movies_Recyclerview);
        button=findViewById(R.id.retry_button);
        textView=findViewById(R.id.retry_textview);

        set_data();

    }

    public void set_data()
    {

        final ProgressDialog progressDialog=new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please Wait!!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient okHttpClient=new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .callTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360,TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        ApiService apiService=retrofit.create(ApiService.class);

        Call<Movies> call=apiService.getPopularMovies(1,"a3cfad4b248c9d56864d174dcd3e8a11");

        call.enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Call<Movies> call, Response<Movies> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().getResults().isEmpty())
                    {
                        if (!response.body().isSuccess())
                        {
                            recyclerView.setVisibility(View.GONE);
                            textView.setVisibility(View.VISIBLE);
                            button.setVisibility(View.VISIBLE);
                            textView.setText(response.body().getStaus_message());
                        }
                        else
                        {
                            recyclerView.setVisibility(View.GONE);
                            textView.setVisibility(View.VISIBLE);
                            button.setVisibility(View.VISIBLE);
                            textView.setText("try Again!!");
                        }
                    }
                    else
                    {
                        MOVIES_DETAILS.clear();
                        MOVIES_DETAILS.addAll(response.body().getResults());
                        moviesListRecyclerAdapter=new MoviesListRecyclerAdapter(MainActivity.this,MOVIES_DETAILS);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                        recyclerView.setAdapter(moviesListRecyclerAdapter);
                        moviesListRecyclerAdapter.notifyDataSetChanged();
                    }
                }
                else
                {
                    Toast.makeText(MainActivity.this, "body getting null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Movies> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Please check your internet connection or give internet permission to app", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void OnClick(View view)
    {
        Intent intent =new Intent(MainActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void recyclerviewclick(int position) {
        Intent intent=new Intent(MainActivity.this,MovieDetailsActivity.class);
        intent.putExtra("back_poster",MOVIES_DETAILS.get(position).getBackdrop_path());
        intent.putExtra("ratings",MOVIES_DETAILS.get(position).getVote_average());
        intent.putExtra("about",MOVIES_DETAILS.get(position).getOverview());
        startActivity(intent);
    }
}
